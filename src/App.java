import com.devcamp.models.Person;
import com.devcamp.models.Staff;
import com.devcamp.models.Student;

public class App {
    public static void main(String[] args) throws Exception {
       Person person1 = new Person("Chung Son", "Hậu Giang");
        System.out.println("person1:");
        System.out.println(person1);

       Person person2 = new Person("Quỳnh Nga", "Bình Tân");
       System.out.println("person2:");
        System.out.println(person2);

        Student student1 = new Student("Chung Son", "Hậu Giang","JAVA DEV",1990,20000.00d);
        System.out.println("student1:");
        System.out.println(student1);

        Student studen2 = new Student("Quỳnh Nga", "Bình Tân","REACT DEV",1995,30000.00d);
        System.out.println("studen2:");
        System.out.println(studen2);

        Staff staff1 = new Staff("Chung Kam", "Hậu Giang","Iron Hack",400000);
        System.out.println("staff1:");
        System.out.println(staff1);

        Staff staff2 = new Staff("Quỳnh Lap", "Bình Tân","Iron Hack",400000);
        System.out.println("staff2:");
        System.out.println(staff2);

    }
}
