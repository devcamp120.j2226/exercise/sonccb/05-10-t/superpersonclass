package com.devcamp.models;

public class Staff extends Person {
 /**
  * Thuộc tính
  */
 private String school;
 private double pay;
/**
 * Phương thức
 */
 public Staff(String name, String address,String school, double pay) {
  super(name, address);
  this.school = school;
  this.pay = pay;
}
/**
 * Getter method
 * @return
 */
public String getSchool() {
 return school;
}

public double getPay() {
 return pay;
}
/**
 * Setter method
 * @param
 */

 public void setSchool(String school) {
  this.school = school;
 }

 public void setPay(double pay) {
  this.pay = pay;
 }

 @Override
 public String toString() {
  return"Staff[Person[name=" + this.getName() + ", address=" + this.getAddress() + "], school=" + this.school + ", pay=" + this.pay + "]";
}
}

