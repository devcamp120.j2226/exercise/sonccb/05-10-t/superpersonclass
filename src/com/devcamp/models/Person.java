package com.devcamp.models;

public class Person {
 private String name;
 private String address;

 public Person(String name, String address) {
  /**
   * Phương thức
   */
  this.name = name;
  this.address = address;
 }
 /**
   * Getter method
   * @return
   */
  public String getName() {
   return name;
  }

  public String getAddress() {
   return address;
  }

  /**
   * Setter method
   * @param
   */

   public void setAddress(String address) {
    this.address = address;
   }

   @Override
   public String toString() {
    
    return "Person[name=" + name + ", address=" + address + "]";
   }
}
